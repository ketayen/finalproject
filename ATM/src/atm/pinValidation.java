/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atm;

import java.util.Scanner;

/**
 *
 * @author ketay
 */
public class pinValidation {
   
    Scanner kb = new Scanner(System.in);
    public  int readInt (String prmptMessage)
      {
         System.out.println(prmptMessage);
         while (!kb.hasNextInt())
         {
            String badInput = kb.nextLine();
            System.out.println("input error. expected an integer. found: " + badInput);
            System.out.println(prmptMessage);
         }
         int n= kb.nextInt();
         return n;
      }

public int readPositiveInt(String promptMessage)
    {

        int x;
        do
        {
           x = readInt(promptMessage);
           if( x < 0 )
           {
            System.out.println("Input Error. Expected a positive integer. Found: " + x);
            System.out.println("Please try again!");
           }
        }while( x < 0 );
        return x;
    }
    
    

}

